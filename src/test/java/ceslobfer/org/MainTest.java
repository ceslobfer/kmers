package ceslobfer.org;



/**
 * Created by César Lobato on 04/04/2016.
 */
public class MainTest {

    public static void main(String[] args) throws Exception {

        long initTime = System.currentTimeMillis();

        //Codones c= new Codones("Llactis_Genes.txt");
        //System.out.println(c.UsoPreferenteDeCodones());
        //c.UsoPreferenteDeCodones();
        //c.GraficaFrecuenciaCodonesAA();
        //c.GraficaUsoPreferenteCodones();
        //c.GraficaUsoPreferenteCodones("C");
        //System.out.println(c.TransferenciaHorizontalGenes());
        //System.out.println(c.Desviacion());



        //c.GraficoAACodonesCompleto();
        Frequency f = new Frequency("Llactis_Genes.txt");
        //System.out.println(f.FrecuenciaPromedioKmers(2));
        //f.GráficaFrecuenciaPromedio(2);
        //Frequency f = new Frequency("Myco01x4.fasta");
        //f.PorcentajeDeParecido();

        //System.out.println(f.KmersFrequency(3));
        //f.PorcentajeDeParecido();
        f.CreaGráficaKmersFrequency(2);
        //Frequency f = new Frequency("E.colik12.fasta");
        long computingTime = System.currentTimeMillis() - initTime ;
        System.out.println(computingTime);
        //f.Kmers(2);

        //Frequency f = new Frequency("UnaSeq.fasta,Prueba.fasta");
        //Frequency f = new Frequency("Myco01.fasta,Myco02.fasta");

        //f.Kmers(10);
        //System.out.println(f.Kmers(2));
        //System.out.println(f.KmersFrequency(2));
        //f.CreaGráficaKmersFrequency(2);
        //f.MostrarFueraDeMedia(2);

        //f.CreaGráficas(2);
        //f.KmersPosFich(2);
        //f.KmersPosFich(2);

        //System.out.print(f.getComplementari());

        //Codones codones= new Codones("E.colik12.fasta,E.coliO157.fasta");
        //Codones codones= new Codones("UnaSeq.fasta,Prueba.fasta");
        //Codones codones= new Codones("Llactis_Genes.txt");
        //Codones codones= new Codones("MusMusculus_chr1.fasta");
        //codones.CodPosFich();
        //System.out.println(codones.CodonesAminoacidos());
        //codones.GraficoAACodonesCompleto();
        //codones.CreaGráficas();
        //System.out.println(codones.FreqCodones());

       // LecturaSeq bj= new LecturaSeq();
        //bj.readFromFastaFile("Llactis_Genes.txt");
        //bj.getNames();


    }
}