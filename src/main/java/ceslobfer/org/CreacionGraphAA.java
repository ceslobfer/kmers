package ceslobfer.org;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.experimental.chart.plot.CombinedCategoryPlot;
import org.jfree.ui.ApplicationFrame;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

/**
 * A demo for the {@link CombinedCategoryPlot} class.por andres2288
 */
public class CreacionGraphAA extends ApplicationFrame {

    private CategoryAxis domainAxis = new CategoryAxis("Category");
    private CombinedCategoryPlot plot = new CombinedCategoryPlot(
            domainAxis, new NumberAxis("Frequencia"));
    public CreacionGraphAA(String title, List<String> filename, List<SortedMap<String,Double>> lsmp, Integer k) {
        super(title);
        DefaultCategoryDataset data1 = createDataset1(filename,lsmp,k);
        DefaultCategoryDataset data2 = createDataset2(filename,lsmp,k);
        CategoryPlot subplot1 = createSubplot1(data1);
        CategoryPlot subplot2 = createSubplot2(data2);
        createPlot(/*subplot1,*/subplot2);
        JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    public  DefaultCategoryDataset createDataset1(List<String> filename, List<SortedMap<String, Double>> lsmp,Integer k) {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        if(k==0) {
            List<String> ls = new ArrayList<String>();
            ls.addAll(filename);
            if (filename.size() > 1) {
                ls.add("Frecuencia media");
            }
            for (int i = 0; i < ls.size(); i++) {
                for (int j = 0; j < lsmp.get(i).keySet().size(); j++) {
                    List<String> keys = new ArrayList<String>();
                    keys.addAll(lsmp.get(i).keySet());
                    result.addValue(lsmp.get(i).get(keys.get(j)), ls.get(i), keys.get(j));
                }
            }
        } else{

            for (int i = 0; i < filename.size(); i++) {
                for (int j = 0; j < lsmp.get(i).keySet().size(); j++) {
                    List<String> keys = new ArrayList<String>();
                    keys.addAll(lsmp.get(i).keySet());
                    result.addValue(lsmp.get(i).get(keys.get(j)), filename.get(i), keys.get(j));
                }
            }
        }
        return result;
    }

    public  DefaultCategoryDataset createDataset2(List<String> filename, List<SortedMap<String, Double>> lsmp,Integer k) {
        DefaultCategoryDataset result = new DefaultCategoryDataset();

        if(k==0) {
            List<String> ls = new ArrayList<String>();
            ls.addAll(filename);
            if (filename.size() > 1) {
                ls.add("Frecuencia media");
            }
            for (int i = 0; i < ls.size(); i++) {
                for (int j = 0; j < lsmp.get(i).keySet().size(); j++) {
                    List<String> keys = new ArrayList<String>();
                    keys.addAll(lsmp.get(i).keySet());
                    result.addValue(lsmp.get(i).get(keys.get(j)), ls.get(i), keys.get(j));
                }
            }
        } else{

            for (int i = 0; i < filename.size(); i++) {
                for (int j = 0; j < lsmp.get(i).keySet().size(); j++) {
                    List<String> keys = new ArrayList<String>();
                    keys.addAll(lsmp.get(i).keySet());
                    result.addValue(lsmp.get(i).get(keys.get(j)), filename.get(i), keys.get(j));
                }
            }
        }
        return result;
    }

    public CategoryPlot createSubplot1(CategoryDataset dataset1) {

        NumberAxis rangeAxis1 = new NumberAxis("Value");
        rangeAxis1.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        LineAndShapeRenderer renderer1 = new LineAndShapeRenderer();
        renderer1.setBaseToolTipGenerator(
                new StandardCategoryToolTipGenerator());
        //Crea suplot1 y lo añade a plot
        CategoryPlot subplot1 = new CategoryPlot(dataset1, null, rangeAxis1,
                renderer1);
        subplot1.setDomainGridlinesVisible(true);
        return subplot1;
    }

    public CategoryPlot createSubplot2(CategoryDataset dataset2) {

        NumberAxis rangeAxis2 = new NumberAxis("Value");
        rangeAxis2.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        BarRenderer renderer2 = new BarRenderer();
        renderer2.setBaseToolTipGenerator(
                new StandardCategoryToolTipGenerator());
        CategoryPlot subplot2 = new CategoryPlot(dataset2, null, rangeAxis2,
                renderer2);
        subplot2.setDomainGridlinesVisible(true);
        return subplot2;
    }

    public void createPlot(/*CategoryPlot subplot1,*/CategoryPlot subplot2) {
       /* plot.add(subplot1, 2);*/
        plot.add(subplot2, 1);
    }

    public JFreeChart createChart(){

        JFreeChart result = new JFreeChart(
                "Grafico comparativo de Kmers",
                new Font("SansSerif", Font.BOLD, 12), this.plot, true);
        return result;

    }

    public  JPanel createDemoPanel() {
        JFreeChart chart = createChart();
        return new ChartPanel(chart);
    }


}