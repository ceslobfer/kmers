package ceslobfer.org;

import javafx.util.Pair;
import org.jfree.ui.RefineryUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by César Lobato on 21/04/2016.
 */
public class Codones {

    private List<char[]> CharArray= new ArrayList<char[]>();
    private List<String> filename= new ArrayList<String>();
    private List<String> cnames= new ArrayList<String>();


    public Codones(String filename) throws IOException {
        List<LecturaSeq> lbj = new ArrayList<LecturaSeq>();
        this.filename= Arrays.asList(filename.split(","));
        for(String file:this.filename) {
            LecturaSeq bj = new LecturaSeq();
            bj.readFromFastaFile(file);
            cnames.addAll(bj.getNames());
            lbj.add(bj);
        }
        for(LecturaSeq bj:lbj){
            for(int i=0;i<bj.getNames().size();i++) {
                CharArray.add(bj.getSequenceData(i));
            }

        }

    }

    public List<SortedMap<String,Double>> NumeroCodones() {
        List<SortedMap<String,Double>> lsm= new ArrayList<SortedMap<String, Double>>();
        for(char[] ca:CharArray) {

            SortedMap<String,Double> sm =new TreeMap<String, Double>();
            Codify codify = new Codify(ca);
            int[] ci = codify.CuentaCodones();
            List<String> ls = new ArrayList<String>();
            ls.addAll(codify.decToChar(3));
            Map<String, Double> mp = new HashMap<String, Double>();

            for (int i = 0; i < ci.length-1; i++) {

                mp.put(ls.get(i), (double) ci[i]);
            }
            sm.putAll(mp);
            lsm.add(sm);
        }
        return lsm;
    }
    public List<SortedMap<String,Double>> FreqCodones()  {
        List<SortedMap<String,Double>> lsm= new ArrayList<SortedMap<String, Double>>();
        for(char[] ca:CharArray) {
            Double tamaño=0.0;
            SortedMap<String,Double> sm =new TreeMap<String, Double>();
            Codify codify = new Codify(ca);
            int[] ci = codify.CuentaCodones();
            List<String> ls = new ArrayList<String>();
            ls.addAll(codify.decToChar(3));
            Map<String, Double> mp = new HashMap<String, Double>();
            for(int ai:ci){
                    tamaño=tamaño+ai;
            }
            for (int i = 0; i < ci.length-1; i++) {
                mp.put(ls.get(i), ((((double )ci[i])/tamaño)));
            }
            sm.putAll(mp);

            lsm.add(sm);
        }
        SortedMap<String,Double> smMedia=new TreeMap<String, Double>();
        smMedia.putAll(lsm.get(0));
        List<String> listaKmers= new ArrayList<String>();
        listaKmers.addAll( lsm.get(0).keySet());
        for(int j=1;j<lsm.size();j++) {

            for (int i = 0; i < listaKmers.size(); i++) {
                Double kmer =0.0;
                kmer=smMedia.get(listaKmers.get(i));
                smMedia.put(listaKmers.get(i), kmer + lsm.get(j).get(listaKmers.get(i)));
            }
        }
        for(int a=0;a<listaKmers.size();a++){
            smMedia.put(listaKmers.get(a),smMedia.get(listaKmers.get(a))/lsm.size());
        }

        lsm.add(smMedia);
        return lsm;
    }
    public void CodPosFich() throws FileNotFoundException {
        List<SortedMap<String, List< Integer>>> listShortedMapPos= new ArrayList<SortedMap<String, List< Integer>>>();
        for(char[] ca:CharArray) {
            Codify codify = new Codify(ca);
            listShortedMapPos.add(codify.posicionCodones());

        }

        File file=new File("Posiciones de Codones");
        PrintWriter ps = new PrintWriter(file);
        ps.println("Posiciones de los codones");
        int i=0;
        for(SortedMap<String, List< Integer>> sortedPrint:listShortedMapPos){
            ps.println(cnames.get(i));
            for(String key:sortedPrint.keySet()){
                ps.println(key+":"+sortedPrint.get(key));
            }
            ps.println("");
            i++;
        }
        ps.close();

    }
    public void CreaGráficaNumeroCodones() throws IOException {
        CreacionGraph cg=new CreacionGraph("Grafica Numero de Codones",cnames, NumeroCodones(),1);
        cg.pack();
        RefineryUtilities.centerFrameOnScreen(cg);
        cg.setVisible(true);

    }
    public void CreaGráficaFreqCodones() throws IOException {
        CreacionGraph cg=new CreacionGraph("Grafica Frequencia de Codones",cnames, FreqCodones(),0);
        cg.pack();
        RefineryUtilities.centerFrameOnScreen(cg);
        cg.setVisible(true);

    }

    public List<SortedMap<String, Double>> FrecuenciaCodonesAA(){
        List<Map<String,List<Pair<String,Double>>>> lca=new ArrayList<Map<String, List<Pair<String, Double>>>>();
        List<SortedMap<String,Double>> lres= new ArrayList<SortedMap<String, Double>>();
        lca=CodonesAminoacidos();
        for (Map<String,List<Pair<String,Double>>> mp:lca){
            SortedMap<String,Double> mpres= new TreeMap<String, Double>();
            for (String aa:mp.keySet()){
                Double count=0.0;
                for (Pair<String,Double> pair:mp.get(aa)){
                    count=count+pair.getValue();
                }
                mpres.put(aa,count);
            }
            lres.add(mpres);
        }
        return lres;
    }

    public List<Pair<String, String>> UsoPreferenteDeCodones() {
        List<Map<String, List<Pair<String, Double>>>> CA = CodonesAminoacidos();
        Map<String, List<Pair<String, Double>>> CAFM = CA.get(CA.size() - 1);
        List<Pair<String,String>> lpreferentes = new ArrayList<Pair<String, String>>();
        for (String s : CAFM.keySet()) {
            String codon="";
            Double count=0.0;
            for (Pair<String, Double> pair : CAFM.get(s)) {
                if (pair.getValue()>count){
                    codon=pair.getKey();
                    count=pair.getValue();
                }
            }
            lpreferentes.add(new Pair<String, String>(codon,s));
        }
        return lpreferentes;
    }

    public List<Pair<String, String>> UsoNoPreferenteDeCodones(){
        List<Map<String, List<Pair<String, Double>>>> CA = CodonesAminoacidos();
        Map<String, List<Pair<String, Double>>> CAFM = CA.get(CA.size() - 1);
        List<Pair<String,String>> lnopreferentes = new ArrayList<Pair<String, String>>();
        for (String s : CAFM.keySet()) {
            String codon="";
            Double count=1.0;
            if(CAFM.get(s).size()>1) {
                for (Pair<String, Double> pair : CAFM.get(s)) {
                    if (pair.getValue() < count) {
                        codon = pair.getKey();
                        count = pair.getValue();
                    }
                }

                lnopreferentes.add(new Pair<String, String>(codon, s));
            }
        }
        return lnopreferentes;
    }
    public List<Map<String, List<Pair<String, String>>>> TransferenciaHorizontalGenes(){
        List<Map<String, List<Pair<String, Double>>>> CA = CodonesAminoacidos();
        List<List<Pair<String,String>>> lcres= new ArrayList<List<Pair<String, String>>>();

        for (Map<String, List<Pair<String, Double>>> CAFM:CA) {
            List<Pair<String,String>> lprovisional= new ArrayList<Pair<String, String>>();

            for (String s : CA.get(CA.size()-1).keySet()) {
                String codon = "";
                Double count = 0.0;
                if(CAFM.get(s).size()>1) {
                    for (Pair<String, Double> pair : CAFM.get(s)) {
                        if (pair.getValue() > count) {
                            codon = pair.getKey();
                            count = pair.getValue();
                        }
                    }
                    lprovisional.add(new Pair<String, String>(codon, s));
                }
            }
            lcres.add(lprovisional);

        }

        List<Pair<String,String>> lnoPreferentes = UsoNoPreferenteDeCodones();
        List<Map<String, List<Pair<String, String>>>> lres= new ArrayList<Map<String, List<Pair<String, String>>>>();
        for (int j=0;j<lcres.size()-1;j++) {
            Map<String, List<Pair<String, String>>> mpres= new HashMap<String, List<Pair<String, String>>>();
            List<Pair<String, String>> lpres=new ArrayList<Pair<String, String>>();
            for (int i = 0; i < lnoPreferentes.size(); i++) {
                if (lcres.get(j).get(i).getKey().equals(lnoPreferentes.get(i).getKey()) && lcres.get(j).get(i).getKey()!=""){
                    lpres.add(lcres.get(j).get(i));
                }
            }
            if(lpres.size()>4) {
                mpres.put(cnames.get(j), lpres);
                lres.add(mpres);
            }
        }
        return lres;
    }

    public void GraficaFrecuenciaCodonesAA(){
        CreacionGraph cg=new CreacionGraph("Grafica Frequencia de Codones por AA",cnames, FrecuenciaCodonesAA(),0);
        cg.pack();
        RefineryUtilities.centerFrameOnScreen(cg);
        cg.setVisible(true);
    }

    public void GraficaUsoPreferenteCodones(){
        List<Map<String,List<Pair<String,Double>>>> lca = new ArrayList<Map<String, List<Pair<String, Double>>>>();
        lca= CodonesAminoacidos();
        int tamaño=lca.size();
        System.out.println(tamaño);
        List<SortedMap<String, Double>> lres= new ArrayList<SortedMap<String, Double>>();
        for(String aa:lca.get(tamaño-1).keySet()){
            SortedMap<String,Double> smres=new TreeMap<String, Double>();
            List<Double> lvalues = new ArrayList<Double>();
            List<String> cnames= new ArrayList<String>();
            Double totalAA=0.0;
            for(Pair<String,Double> pair:lca.get(tamaño-1).get(aa)){
                totalAA=totalAA+pair.getValue();
                lvalues.add(pair.getValue());
                cnames.add(pair.getKey());
            }
            for (int i=0;i<lvalues.size();i++){
                Double r=lvalues.get(i)/totalAA;
                smres.put(cnames.get(i),r);
            }
            lres.add(smres);
        }
        List<String> lset= new ArrayList<String>();
        lset.addAll(lca.get(tamaño-1).keySet());
        for(int i=0;i<lset.size();i++) {
            if (lres.get(i).size()>1) {
                List<String> lonestring = new ArrayList<String>();
                lonestring.add(lset.get(i));
                List<SortedMap<String, Double>> lresone = new ArrayList<SortedMap<String, Double>>();
                lresone.add(lres.get(i));
                CreacionGraphAA cg = new CreacionGraphAA("Grafica Uso preferente de codones", lonestring, lresone, 1);
                cg.pack();
                RefineryUtilities.centerFrameOnScreen(cg);
                cg.setVisible(true);
            }
        }
    }

    public void GraficaUsoPreferenteCodones(String AA){
        List<Map<String,List<Pair<String,Double>>>> lca = new ArrayList<Map<String, List<Pair<String, Double>>>>();
        lca= CodonesAminoacidos();
        int tamaño=lca.size();
        System.out.println(tamaño);
        List<SortedMap<String, Double>> lres= new ArrayList<SortedMap<String, Double>>();
        for(String aa:lca.get(tamaño-1).keySet()){
            if(AA.equals(aa)) {
                SortedMap<String, Double> smres = new TreeMap<String, Double>();
                List<Double> lvalues = new ArrayList<Double>();
                List<String> cnames = new ArrayList<String>();
                Double totalAA = 0.0;
                for (Pair<String, Double> pair : lca.get(tamaño - 1).get(aa)) {
                    totalAA = totalAA + pair.getValue();
                    lvalues.add(pair.getValue());
                    cnames.add(pair.getKey());
                }
                for (int i = 0; i < lvalues.size(); i++) {
                    Double r = lvalues.get(i) / totalAA;
                    smres.put(cnames.get(i), r);
                }
                lres.add(smres);
            }
        }
        List<String> lset= new ArrayList<String>();
        lset.addAll(lca.get(tamaño-1).keySet());
        for(int i=0;i<lres.size();i++) {
            List<String> lonestring=new ArrayList<String>();
            lonestring.add(lset.get(i));
            List<SortedMap<String, Double>> lresone= new ArrayList<SortedMap<String, Double>>();
            lresone.add(lres.get(i));
            CreacionGraphAA cg = new CreacionGraphAA("Grafica Uso preferente de codones", lonestring,lresone, 1);
            cg.pack();
            RefineryUtilities.centerFrameOnScreen(cg);
            cg.setVisible(true);
        }
    }
    public List<Map<String,List<Pair<String,Double>>>> CodonesAminoacidos(){

        List<SortedMap<String,Double>> lsmf = new ArrayList<SortedMap<String, Double>>();
        lsmf.addAll(FreqCodones());
        int matrizCoincidentes [][] = new int [22] [64];
        List<String> lc= new ArrayList<String>();
        lc.addAll(lsmf.get(0).keySet());
        String [] aa= {"K","N","K","N","T","T","T","T","R","S","R","S","I","I","M","I","Q","H","Q","H",
                "P","P","P","P","R","R","R","R","L","L","L","L","E","D","E","D","A","A","A","A","G","G",
                "G","G","V","V","V","V","*","Y","*","Y","S","S","S","S","U","C","W","C","L","F","L","F"};
        List<Map<String,List<Pair<String,Double>>>> lmp= new ArrayList<Map<String, List<Pair<String, Double>>>>();
        for(int j=0;j<lsmf.size();j++) {
            Map<String,List<Pair<String,Double>>> mp= new HashMap<String, List<Pair<String, Double>>>();
            lmp.add(mp);
            for (int i = 0; i < aa.length; i++) {
                if ( lmp.get(j).containsKey(aa[i]) ) {
                    lmp.get(j).get(aa[i]).add(new Pair<String, Double>(lc.get(i),lsmf.get(j).get(lc.get(i))));
                }else{
                    List<Pair<String,Double>> lp= new ArrayList<Pair<String, Double>>();
                    lp.add(new Pair<String,Double>(lc.get(i),lsmf.get(j).get(lc.get(i))));
                    lmp.get(j).put(aa[i],lp);
                }
            }
        }

        return lmp;

    }

    public void GraficoAACodonesCompleto(){
        for (int i=0;i<cnames.size();i++) {
            BarrasGraph v = new BarrasGraph(CodonesAminoacidos(), cnames.get(i),i);
            v.setVisible(true);
        }

    }

}
