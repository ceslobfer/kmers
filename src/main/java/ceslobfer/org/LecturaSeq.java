package ceslobfer.org;

import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static org.biojava.nbio.core.sequence.io.FastaWriterHelper.writeProteinSequence;


public class LecturaSeq {
    private LinkedHashMap<String, ProteinSequence> a;
    String filename;
    private List<String> dataseq= new ArrayList<String>();

    public List<String> getNames() {
        List<String> listName = new ArrayList<String>();
        List<String> ln = new ArrayList<String>();

        String[] file=this.filename.split("\\.");
        if(file[file.length-1].equals("fasta")) {
            ln.addAll(a.keySet());
            if(ln.size()>200){
                for(int i=0;i<200;i++){
                    listName.add(ln.get(i));
                }
            }else {
                listName.addAll(ln);
            }
        }else if(file[file.length-1].equals("txt")){
            ln.addAll(a.keySet());
            if (ln.size()>200){
                for(int i=0;i<200;i++){
                    listName.add(ln.get(i).split("\\+")[0].split("\\-")[0]);

                }
            }else {
                for (String sa : ln) {
                    listName.add(sa.split("\\+")[0].split("\\-")[0]);
                }
            }

        }
        return listName;

    }

    public char[] getSequenceData(int sequenceIndex) {
        int i=0;
        for(ProteinSequence b:this.a.values()){
            i++;
            dataseq.add(b.toString());
            /*if(i==2000){
                break;
            }*/
        }

        return dataseq.get(sequenceIndex).toCharArray();
    }

    public void readFromFastaFile(String fileName) throws IOException {
        this.filename=fileName;
        this.a = FastaReaderHelper.readFastaProteinSequence(new File(fileName));

        /*La lectura del fichero usando readFastaProteinSequence, tiene una estructura
         * valida tanto para proteinas con sus respectivas secuencias de aminoácidos y
          * su descripcion como para genes con su secuencia de nucleótidos y su descripción*/

    }

    public void writeToFastaFile(String fileName) throws Exception {
        writeProteinSequence(new  File(fileName), a.values());

    }

    public void readFromUniProt(List<String> unitProtId) {
        for(String fileName:unitProtId) {
            try {
                String servicio = "http://www.uniprot.org";
                URL url = new URL(servicio + "/uniprot/" + fileName + ".fasta");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);

                System.out.println(connection.getContentType());

                InputStream is = connection.getInputStream();
                FileOutputStream f = new FileOutputStream("C:\\Users\\ceslo\\Downloads\\FASTAS\\" + fileName + ".fasta");

                byte[] array = new byte[1000];
                int leido = is.read(array);
                while (leido > 0) {
                    f.write(array, 0, leido);
                    leido = is.read(array);
                }
                is.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
