package ceslobfer.org;


import javafx.util.Pair;
import org.jfree.ui.RefineryUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.List;

import static java.lang.Math.sqrt;

public class Frequency {

    private List<char[]> CharArray= new ArrayList<char[]>();
    private List<String> filename= new ArrayList<String>();
    private List<String> knames= new ArrayList<String>();

    public Frequency(String filename) throws IOException {
        List<LecturaSeq> lbj = new ArrayList<LecturaSeq>();
        this.filename=Arrays.asList(filename.split(","));
        for(String file:this.filename) {
            LecturaSeq bj = new LecturaSeq();
            bj.readFromFastaFile(file);
            lbj.add(bj);
            knames.addAll(bj.getNames());
        }
        for(LecturaSeq bj:lbj){
            for(int i=0;i<bj.getNames().size();i++) {
                CharArray.add(bj.getSequenceData(i));
            }
        }
    }

    public List<String> NucleotidFrequency() throws IOException {
        List<String> res= new ArrayList<String>();
        for(char[] CA:CharArray) {
            Double A = 0.0, C = 0.0, T = 0.0, G = 0.0;

            for (char c : CA) {
                if (c == 'A') {
                    A++;
                } else if (c == 'C') {
                    C++;
                } else if (c == 'G') {
                    G++;
                } else {
                    T++;
                }
            }
            res.add("< A: " + ((A/CA.length)*100) + ", C: " + ((C/CA.length)*100)+ ", T: " +
                    ((T/CA.length)*100) + ", G: " + ((G/CA.length)*100) + ">");
        }
        return res;
    }
    public List<List<Character>> getComplementari(){
        List<List<Character>> listChar= new ArrayList<List<Character>>();
        for(int j=0;j<CharArray.size();j++){
            List<Character> lc= new ArrayList<Character>();
            listChar.add(lc);
            for(int i=0;i<CharArray.get(j).length;i++){
                if(CharArray.get(j)[i]=='A'){
                    listChar.get(j).add('T');
                }else if(CharArray.get(j)[i]=='C'){
                    listChar.get(j).add('G');
                }else if(CharArray.get(j)[i]=='T'){
                    listChar.get(j).add('A');
                }else if(CharArray.get(j)[i]=='G'){
                    listChar.get(j).add('C');
                }
            }
        }

        return listChar;
    }

    public List<SortedMap<String,Double>> Kmers(int k) {
        List<SortedMap<String,Double>> lsm= new ArrayList<SortedMap<String, Double>>();
        for(char[] ca:CharArray) {

            SortedMap<String,Double> sm =new TreeMap<String, Double>();
            Codify codify = new Codify(ca);
            int[] ci = codify.CuentaKmers(k);
            List<String> ls = new ArrayList<String>();
            ls.addAll(codify.decToChar(k));
            Map<String, Double> mp = new HashMap<String, Double>();

            for (int i = 0; i < ci.length-1; i++) {
                mp.put(ls.get(i), (double) ci[i]);
            }
            sm.putAll(mp);
            lsm.add(sm);
        }

        return lsm;
    }
    public List<SortedMap<String,Double>> KmersFrequency(int k){
        List<SortedMap<String,Double>> lsm= new ArrayList<SortedMap<String, Double>>();
        for(char[] ca:CharArray) {
            Double tamaño=0.0;

            SortedMap<String,Double> sm =new TreeMap<String, Double>();
            Codify codify = new Codify(ca);
            int[] ci = codify.CuentaKmers(k);
            List<String> ls = new ArrayList<String>();
            ls.addAll(codify.decToChar(k));
            Map<String, Double> mp = new HashMap<String, Double>();
            for(int ai:ci){
                tamaño=tamaño+ai;
            }
            for (int i = 0; i < ci.length-1; i++) {
                mp.put(ls.get(i),  ((((double )ci[i])/(ca.length-1))));
            }
            sm.putAll(mp);
            lsm.add(sm);
        }
        if (lsm.size() > 1) {
            SortedMap<String, Double> smMedia = new TreeMap<String, Double>();
            smMedia.putAll(lsm.get(0));
            List<String> listaKmers = new ArrayList<String>();
            listaKmers.addAll(lsm.get(0).keySet());
            for (int j = 1; j < lsm.size(); j++) {

                for (String listaKmer : listaKmers) {
                    Double kmer = smMedia.get(listaKmer);
                    smMedia.put(listaKmer, kmer + lsm.get(j).get(listaKmer));
                }
            }
            for (String listaKmer : listaKmers) {
                smMedia.put(listaKmer, smMedia.get(listaKmer) / lsm.size());
            }

            lsm.add(smMedia);
        }
        return lsm;

    }

    public SortedMap<String, Double> FrecuenciaPromedioKmers(int k){
        List<SortedMap<String,Double>> KF=KmersFrequency(k);
        return(KF.get(KF.size()-1));
    }

    public void GráficaFrecuenciaPromedio(int k){
        List<SortedMap<String,Double>> lg= new ArrayList<SortedMap<String, Double>>();
        lg.add(FrecuenciaPromedioKmers(k));
        List<String> ln= new ArrayList<String>();
        ln.add("Frecuencia Promedio");
        CreacionGraph cg=new CreacionGraph("Grafica Kmers",ln, lg,1);
        cg.pack();
        RefineryUtilities.centerFrameOnScreen(cg);
        cg.setVisible(true);
    }

    public void CreaGráficaKmers(Integer k) {
        CreacionGraph cg=new CreacionGraph("Grafica Kmers",knames, Kmers(k),1);
        cg.pack();
        RefineryUtilities.centerFrameOnScreen(cg);
        cg.setVisible(true);

    }
    public void CreaGráficaKmersFrequency(Integer k) {
        List<SortedMap<String,Double>> kf= new ArrayList<SortedMap<String, Double>>();
        kf=KmersFrequency(k);
        CreacionGraph cg=new CreacionGraph("Grafica Frequencia de Kmers",knames, kf,0);
        cg.pack();
        RefineryUtilities.centerFrameOnScreen(cg);
        cg.setVisible(true);

    }

    private List<SortedMap<String,List< Integer>>> KmersPos(Integer k){
        List<SortedMap<String, List< Integer>>> listShortedMapPos= new ArrayList<SortedMap<String, List< Integer>>>();
        for(char[] ca:CharArray) {
            Codify codify = new Codify(ca);

            listShortedMapPos.add(codify.posicionKmers(k));
        }
        return listShortedMapPos;
    }
    public void KmersPosFich(Integer k) throws FileNotFoundException {
        List<SortedMap<String, List< Integer>>> listShortedMapPos= new ArrayList<SortedMap<String, List< Integer>>>();
        for(char[] ca:CharArray) {
            Codify codify = new Codify(ca);
            listShortedMapPos.add(codify.posicionKmers(k));

        }

        File file=new File("Posiciones");
        PrintWriter ps = new PrintWriter(file);
        int i=0;
        for(SortedMap<String, List< Integer>> sortedPrint:listShortedMapPos){
            ps.println(knames.get(i));
            for(String key:sortedPrint.keySet()){
                ps.println(key+":"+sortedPrint.get(key));
            }
            ps.println("");
            i++;
        }
        ps.close();



    }


    public void PorcentajeDeParecido() throws FileNotFoundException {
        List<Pair<List<String>,Double>> lpairf= DiferenciaPorcentajes(KmersFrequency(2));

        File file=new File("PorcentajesParecidosSegunFreq");

        EscribePorcentajes(lpairf, file);
    }

    private void EscribePorcentajes(List<Pair<List<String>, Double>> lpairn, File file1) throws FileNotFoundException {
        PrintWriter ps1 = new PrintWriter(file1);
        for(String s:knames){
            ps1.println("Porcentaje de parecido con respecto a "+s+":");
            for(Pair<List<String>,Double> pair:lpairn){
                if(pair.getKey().contains(s) && pair.getKey().indexOf(s)==0){
                    ps1.println(pair.getKey().get(1).trim()+"="+pair.getValue()*100);
                }else if(pair.getKey().contains(s) && pair.getKey().indexOf(s)==1){
                    ps1.println(pair.getKey().get(0).trim()+"="+pair.getValue()*100);
                }
            }
            ps1.println();
        }
        ps1.close();
    }

    private List<Pair<List<String>,Double>> DiferenciaPorcentajes(List<SortedMap<String,Double>> lmsf){
        List<String> ls= new ArrayList<String>();
        List<Pair<List<String>,Double>> pairList= new ArrayList<Pair<List<String>, Double>>();
        ls.addAll(lmsf.get(0).keySet());
        List<String> lnames=knames;
        lnames.add("FrecuenciaMedia");
        for(int i=0;i<lmsf.size()-1;i++){
            for(int j=i+1;j<lmsf.size();j++){
                Double resp=0.0;

                for(String k:ls){
                    if(lmsf.get(i).get(k)-lmsf.get(j).get(k)<0){
                        resp=resp +(lmsf.get(i).get(k)/lmsf.get(j).get(k));
                    }else{
                        resp=resp+(lmsf.get(j).get(k)/lmsf.get(i).get(k));
                    }
                }
                List<String> namesDef= new ArrayList<String>();
                namesDef.add(lnames.get(i));
                namesDef.add(lnames.get(j));
                pairList.add(new Pair<List<String>,Double>(namesDef,resp/16));

            }
        }
        return pairList;
    }





}

