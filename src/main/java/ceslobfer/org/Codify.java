package ceslobfer.org;

import java.util.*;

import static java.lang.Math.pow;

/**
 * Created by César Lobato on 11/04/2016.
 */
public class Codify {

    private char [] CharArray;
    private int [] ci;



    public Codify(char[] CA){
        CharArray=CA;
    }

    public int [] CuentaKmers(int k){
        List<Character> lc= new ArrayList<Character>();
        Integer tamaño=(int)pow(4,k);
        ci = new int[tamaño+1];
        int [] asci= new int[('T'-'A')+1];
        asci ['A'-'A']=0;
        asci ['C'-'A']=1;
        asci ['G'-'A']=2;
        asci ['T'-'A']=3;

        for(char c:CharArray){
            lc.add(c);
        }
        for(int j=0; j<(lc.size()-(k-1));j++){
            Integer cont=0;

            for(int i=j; i<(j+k);i++){
                Integer a=i-j;
                int mult=asci[lc.get(i)-'A'];
                cont= cont + mult*(int) pow(10, a);

            }
            ci [Integer.parseInt(cont.toString(),4)]= ci [Integer.parseInt(cont.toString(),4)]+1;
        }
        return ci;
    }
    public SortedMap<String,List<Integer>> posicionKmers(Integer k){
        SortedMap<String,List<Integer>> SortPos= new TreeMap<String, List<Integer>>();
        List<String> lpos= new ArrayList<String>();
        CuentaKmers(k);
        lpos=decToChar(k);
        Map<String,List<Integer>> MapPos= new HashMap<String, List<Integer>>();
        List<Character> lc= new ArrayList<Character>();
        for(char c:CharArray){
            lc.add(c);
        }
        for(int j=0; j<(lc.size()-(k-1));j++){
            Integer cont=0;

            for(int i=j; i<(j+k);i++){
                Integer a=i-j;

                if(lc.get(i).equals('A')){
                    cont= cont + 0;
                }else if(lc.get(i).equals('C')){
                    cont= cont + (int) pow(10, a);
                }else if(lc.get(i).equals('G')){
                    cont= cont + 2*(int)pow(10,a);
                }else if(lc.get(i).equals('T')){
                    cont= cont + 3*(int)pow(10,a);
                }
            }
            if(MapPos.containsKey(lpos.get(Integer.parseInt(cont.toString(), 4)))){
                MapPos.get(lpos.get(Integer.parseInt(cont.toString(),4))).add(j);
            }else {
                List<Integer> lpp=new ArrayList<Integer>();
                lpp.add(j);
                MapPos.put(lpos.get(Integer.parseInt(cont.toString(), 4)),lpp );
            }
        }
        SortPos.putAll(MapPos);
        return SortPos;


    }

    public SortedMap<String,List<Integer>> posicionCodones(){
        SortedMap<String,List<Integer>> SortPos= new TreeMap<String, List<Integer>>();
        List<String> lpos= new ArrayList<String>();
        CuentaCodones();
        lpos=decToChar(3);
        Map<String,List<Integer>> MapPos= new HashMap<String, List<Integer>>();
        List<Character> lc= new ArrayList<Character>();

        for(char c:CharArray){
            lc.add(c);
        }
        for(int j=0; j<(lc.size()-(2));j++){
            Integer cont=0;

            for(int i=j; i<(j+3);i++){
                Integer a=i-j;

                if(lc.get(i).equals('A')){
                    cont= cont + 0;
                }else if(lc.get(i).equals('C')){
                    cont= cont + (int) pow(10, a);
                }else if(lc.get(i).equals('G')){
                    cont= cont + 2*(int)pow(10,a);
                }else if(lc.get(i).equals('T')){
                    cont= cont + 3*(int)pow(10,a);
                }
            }
            if(MapPos.containsKey(lpos.get(Integer.parseInt(cont.toString(), 4))) && j%3==0 ){
                MapPos.get(lpos.get(Integer.parseInt(cont.toString(),4))).add(j);
            }else if(j%3==0){
                List<Integer> lpp=new ArrayList<Integer>();
                lpp.add(j);
                MapPos.put(lpos.get(Integer.parseInt(cont.toString(), 4)),lpp );
            }
        }
        SortPos.putAll(MapPos);
        return SortPos;


    }

    public  List<String> decToChar(int k){
        List<String> ls= new ArrayList<String>();
        for(int pos=0;pos<(ci.length-1);pos++) {

            String res = "";
            String posF = Integer.toString(pos, 4);
            int size = posF.length();
            String[] cp = posF.split("//|");
            for (int i = cp.length - 1; i >= 0; i--) {
                if (cp[i].equals("0")) {
                    res = res + 'A';
                } else if (cp[i].equals("1")) {
                    res = res + 'C';
                } else if (cp[i].equals("2")) {
                    res = res + 'G';
                } else if (cp[i].equals("3")) {
                    res = res + 'T';
                }
            }

            while (size < k) {
                res = res + "A";
                size++;
            }

            ls.add(res);
        }
        return ls;

    }

    public int[] CuentaCodones(){
        List<Character> lc= new ArrayList<Character>();
        Integer tamaño=(int)pow(4,3);
        ci = new int[tamaño+1];
        int [] asci= new int[('T'-'A')+1];
        asci ['A'-'A']=0;
        asci ['C'-'A']=1;
        asci ['G'-'A']=2;
        asci ['T'-'A']=3;

        for(char c:CharArray){
            lc.add(c);
        }
        for(int j=0; j<(lc.size()-(3-1));j=j+3){
            Integer cont=0;

            for(int i=j; i<(j+3);i++){
                Integer a=i-j;
                int mult=asci[lc.get(i)-'A'];
                cont= cont + mult*(int) pow(10, a);

            }
            ci [Integer.parseInt(cont.toString(),4)]= ci [Integer.parseInt(cont.toString(),4)]+1;
        }
        return ci;
    }
}
