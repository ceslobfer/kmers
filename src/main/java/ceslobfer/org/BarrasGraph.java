package ceslobfer.org;

/**
 * Created by César Lobato on 27/04/2016.
 */

        import java.awt.*;
        import java.util.*;
        import java.util.List;
        import javax.swing.*;

        import javafx.util.Pair;
        import org.jfree.chart.ChartFactory;
        import org.jfree.chart.ChartPanel;
        import org.jfree.chart.JFreeChart;
        import org.jfree.chart.plot.CategoryPlot;
        import org.jfree.chart.plot.PlotOrientation;
        import org.jfree.data.category.DefaultCategoryDataset;

public class BarrasGraph extends JFrame {
        private List<Map<String,List<Pair<String,Double>>>> lmp=
                new ArrayList<Map<String, List<Pair<String, Double>>>>();

        public BarrasGraph(List<Map<String,List<Pair<String,Double>>>> lmp, String name,int i) {
            this.lmp=lmp;
            setTitle("Frecuencias de Codones");
            setSize(800, 600);
            setLocationRelativeTo(null);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setVisible(true);

                JPanel panel=new JPanel();
                init(getDataset(i),name,panel);

        }
        private DefaultCategoryDataset getDataset(int i){
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();

                    for (String key : lmp.get(i).keySet()) {
                        for (int j = 0; j < lmp.get(i).get(key).size(); j++)
                            dataset.setValue(lmp.get(i).get(key).get(j).getValue(),
                                    key, lmp.get(i).get(key).get(j).getKey());
                    }


            return dataset;

        }
        private void init(DefaultCategoryDataset dataset,String name,JPanel panel) {
            getContentPane().add(panel);
            // Fuente de Datos

            // Creando el Grafico
            JFreeChart chart = ChartFactory.createBarChart3D
                    (name, "Codones", "Frecuencias de codones",
                            dataset, PlotOrientation.VERTICAL, true, true, false);
            chart.setBackgroundPaint(Color.cyan);
            chart.getTitle().setPaint(Color.black);
            CategoryPlot p = chart.getCategoryPlot();
            p.setRangeGridlinePaint(Color.red);
            // Mostrar Grafico
            ChartPanel chartPanel = new ChartPanel(chart);
            panel.add(chartPanel);
        }


    }

